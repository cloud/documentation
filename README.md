# Metacentrum Cloud Documentation


**Note that this documentation is getting obsolete, use [e-infra_cz doc repo](https://gitlab.ics.muni.cz/einfra-docs/documentation) instead.**

## Run development server

```
hugo serve --config config-dev.toml --bind 127.0.0.1 --port 1313
```
