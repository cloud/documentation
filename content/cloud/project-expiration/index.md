---
title: "Project Expiration Policy"
date: 2021-06-18T11:22:35+02:00
draft: false
disableToC: true
GeekdocHidden: true
---

Every group project has its expiration date. The date is set when creating the project. Expired project are to be disabled and its data later removed.

When the project expires, we will contact its owner who can either extend project's expiration date or confirm its expiration. You have *two weeks* to respond to this e-mail. After *two weeks* the project will be disabled. After another *month* all resources including data will be removed permanently.
