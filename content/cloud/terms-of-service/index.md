---
title: "Terms of Service"
date: 2021-07-21T09:22:35+02:00
draft: false
weight: 200
disableToc: true
---

### *The following documents describe your rights and responsibilities as a user of MetaCentrum Cloud.*

- Terms and conditions for the access to the CESNET e-infrastructure: [EN](https://www.cesnet.cz/conditions/?lang=en) | [CZ](https://www.cesnet.cz/podminky/)
- NGI MetaCentrum - Rules of Use: [EN](https://www.metacentrum.cz/en/about/rules/index.html) | [CZ](https://www.metacentrum.cz/cs/about/rules/index.html)
- Masaryk University Directive No. 10/2017 Use of information technology: [EN](https://is.muni.cz/do/mu/Uredni_deska/Predpisy_MU/Masarykova_univerzita/Smernice_MU/SM10-17/102278820/MU_Directive_No._10_2017_-_Use_of_Information_Technology.pdf) | [CZ](https://is.muni.cz/do/mu/Uredni_deska/Predpisy_MU/Masarykova_univerzita/Smernice_MU/SM10-17/102278820/Smernice_MU_c.10_2017_-_Pouzivani_informacnich_technologii__ucinna_od_15.6.2020_.pdf)



### *Beside mentioned document, following rules and information apply.*

#### Backups

- We are not responsible for any data loss.
- Users are responsible for making backups.

#### Floating IPs

- Allocated PERSONAL scope addresses that are not assigned to any VM are deallocated daily.
- Allocated GROUP scope addresses that are not assigned to any VM can be deallocated after 3 months.
