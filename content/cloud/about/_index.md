---
title: "About metacentrum.cz cloud"
date: 2021-05-18T11:22:35+02:00
draft: false
weight: -100
GeekdocHidden: true
---

## Hardware
MetaCentrum Cloud consists of 17 computational clusters containing 277 hypervisors
with a sum of 8968 cores, 96 GPU cards, and 178 TB RAM. Special demand applications can utilize our clusters with local SSDs and GPU cards. OpenStack instances, object storage and image storage
can leverage more than 1.5 PTB of highly available data storage provided by the CEPH storage system.

## Software

MetaCentrum Cloud is built on top of OpenStack, which is a free open standard cloud computing platform
and one of the top 3 most active open source projects in the world. New OpenStack major versions are
released twice a year. OpenStack functionality is separated into more than 50 services.

## Application
More than 400 users are using the MetaCentrum Cloud platform and more than 130k VMs were started last year.

## MetaCentrum Cloud current release

OpenStack Train

## Deployed services

The following table contains a list of OpenStack services deployed in MetaCentrum Cloud. Services are separated
into two groups based on their stability and the level of support we are able to provide. All services in the production
group are well tested by our team and are covered by the support of cloud@metacentrum.cz. To be able to support
a variety of experimental cases we are planning to deploy several services as experimental, which can be useful
for testing purposes, but its functionality won't be covered by the support of cloud@metacentrum.cz.

| Service   | Description            | Type         |
|-----------|------------------------|--------------|
| cinder    | Block Storage service  | production   |
| glance    | Image service          | production   |
| heat      | Orchestration service  | production   |
| horizon   | Dashboard              | production   |
| keystone  | Identity service       | production   |
| neutron   | Networking service     | production   |
| nova      | Compute service        | production   |
| octavia   | Load Balancing Service | experimental |
| placement | Placement service      | production   |
| swift/s3  | Object Storage service | production   |
