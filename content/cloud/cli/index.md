---
title: "Command Line Interface"
date: 2021-05-18T11:22:35+02:00
draft: false
---

In order to have access to OpenStack's API, you have to use so-called OpenStack Application Credentials. In short,
it is a form of token-based authentication providing easy and secure access without the use of passwords.

## Getting Credentials
1. In **Identity &gt; Application Credentials**, click on **Create Application Credential**.
2. Choose name, description and expiration date & time.

  ![](images/app_creds_1.png)

{{< hint info >}}
**Notice:**

Do NOT select specific roles, unless directed otherwise by user support.
{{< /hint >}}

{{< hint info >}}
**Notice:**

If you decide to select specific roles, you should always include at least the **member** role.
If you are planning to use the orchestration API, add the **heat_stack_owner** role as well and
check **Unrestricted**.
{{< /hint >}}

3. Download provided configuration files for the OpenStack CLI client.

  ![](images/app_creds_2.png)

## Setting Up
1. [Install](https://pypi.org/project/python-openstackclient/) and
   [configure](https://docs.openstack.org/python-openstackclient/train/configuration/index.html)
   OpenStack CLI client.

{{< hint danger >}}
**WARNING:**

Add the following line to the **openrc** file:

`export OS_VOLUME_API_VERSION=3`

Add the following line to the **clouds.yaml** file:

`volume_api_version: 3`
{{< /hint >}}


2. Follow the official [Launch instances](https://docs.openstack.org/nova/train/user/launch-instances.html) guide.

---


## Creating a key-pair

You can either get your private key from the dashboard or you can use **ssh-keygen** command to create a new private key:

```
ssh-keygen -b 4096
```
then you will be asked to specify the output file and passphrase for your key.


1. Assuming your ssh public key is stored in `~/.ssh/id_rsa.pub`
```
openstack keypair create --public-key ~/.ssh/id_rsa.pub my-key1
```

## Create a security group
1. Create:
```
openstack security group create my-security-group
```

2. Add rules to your security group:
```
openstack security group rule create --description "Permit SSH" --remote-ip 0.0.0.0/0 --protocol tcp --dst-port 22 --ingress my-security-group
openstack security group rule create --description "Permit ICMP (any)" --remote-ip 0.0.0.0/0 --protocol icmp --icmp-type -1 --ingress my-security-group
```

3. Verify:
```
 openstack security group show my-security-group
```

## Create a network

1. Create network + subnet (from an auto-allocated pool)
```
openstack network create my-net1
openstack subnet create --network my-net1 --subnet-pool private-192-168 my-sub1
```

##Router management

### Router Creation

2. Create router:
```
openstack router create my-router1
```
The current router has no ports, which makes it pretty useless, we need to create at least 2 interfaces (external and internal)

3. Set external network for the router (let us say public-muni-147-251-124), and the external port will be created automatically:
```
openstack router set --external-gateway public-muni-147-251-124 my-router1
```

4. Check which IP address is set as gateway for our subnet (default: first address of the subnet):
```
GW_IP=$(openstack subnet show my-sub1 -c gateway_ip -f value)
```

5. Create an internal port for the router (gateway for the network my-net1):
```
openstack port create --network my-net1 --disable-port-security --fixed-ip ip-address=$GW_IP my-net1-port1-gw
```

6. Add port to the router:
```
openstack router add port my-router1 my-net1-port1-gw
```
### Clear gateway

1. Find your router:
```
$  openstack router list
+--------------------------------------+-----------------------+--------+-------+-------------+------+----------------------------------+
| ID                                   | Name                  | Status | State | Distributed | HA   | Project                          |
+--------------------------------------+-----------------------+--------+-------+-------------+------+----------------------------------+
| 0bd0374d-b62e-429a-8573-3e8527399b68 | auto_allocated_router | ACTIVE | UP    | None        | None | f0c339b86ddb4699b6eab7acee8d4508 |
+--------------------------------------+-----------------------+--------+-------+-------------+------+----------------------------------+
```

2. Verify:
```
$ openstack router show 0bd0374d-b62e-429a-8573-3e8527399b68
+-------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field                   | Value                                                                                                                                                                                       |
+-------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| admin_state_up          | UP                                                                                                                                                                                          |
| availability_zone_hints | None                                                                                                                                                                                        |
| availability_zones      | None                                                                                                                                                                                        |
| created_at              | 2019-06-06T04:47:15Z                                                                                                                                                                        |
| description             | None                                                                                                                                                                                        |
| distributed             | None                                                                                                                                                                                        |
| external_gateway_info   | {"network_id": "8d5e18ab-5d43-4fb5-83e9-eb581c4d5365", "enable_snat": true, "external_fixed_ips": [{"subnet_id": "41e0cd1c-5ab8-465f-8605-2e7d6a3fe5b4", "ip_address": "147.251.124.177"}]} |
| flavor_id               | None                                                                                                                                                                                        |
| ha                      | None                                                                                                                                                                                        |
| id                      | 0bd0374d-b62e-429a-8573-3e8527399b68                                                                                                                                                        |
| interfaces_info         | [{"port_id": "92c3f6fe-afa8-47c6-a1a6-f6a1b3c54f72", "ip_address": "192.168.8.193", "subnet_id": "e903d5b9-ac90-4ca8-be2c-c509a0153982"}]                                                   |
| location                | Munch({'cloud': '', 'region_name': 'brno1', 'zone': None, 'project': Munch({'id': 'f0c339b86ddb4699b6eab7acee8d4508', 'name': None, 'domain_id': None, 'domain_name': None})})              |
| name                    | auto_allocated_router                                                                                                                                                                       |
| project_id              | f0c339b86ddb4699b6eab7acee8d4508                                                                                                                                                            |
| revision_number         | 24                                                                                                                                                                                          |
| routes                  |                                                                                                                                                                                             |
| status                  | ACTIVE                                                                                                                                                                                      |
| tags                    |                                                                                                                                                                                             |
| updated_at              | 2019-06-06T06:34:34Z                                                                                                                                                                        |
+-------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
```
3. Unset gateway (by ID of the router):

```
$ openstack router unset --external-gateway 0bd0374d-b62e-429a-8573-3e8527399b68
```

### Set Gateway

1. Choose a new external network:

```
$ openstack network list
+--------------------------------------+--------------------------+--------------------------------------+
| ID                                   | Name                     | Subnets                              |
+--------------------------------------+--------------------------+--------------------------------------+
| 410e1b3a-1971-446b-b835-bf503917680d | public-cesnet-78-128-251 | 937106e2-3d51-43cc-83b6-c779465011e5 |
| 8d5e18ab-5d43-4fb5-83e9-eb581c4d5365 | public-muni-147-251-124  | 41e0cd1c-5ab8-465f-8605-2e7d6a3fe5b4 |
| c708270d-0545-4be2-9b8f-84cf75ce09cf | auto_allocated_network   | e903d5b9-ac90-4ca8-be2c-c509a0153982 |
| d896044f-90eb-45ee-8cb1-86bf8cb3f9fe | private-muni-10-16-116   | 3d325abf-f9f8-4790-988f-9cd3d1dea4f3 |
+--------------------------------------+--------------------------+--------------------------------------+
```

2. Set the new external network for the router

```
$ openstack router set --external-gateway public-cesnet-78-128-251  0bd0374d-b62e-429a-8573-3e8527399b68
```


## Create volume
{{< hint danger >}}
**WARNING**

Skipping this section can lead to unreversible loss of data
{{</hint>}}

Volumes are created automatically when creating an instance in GUI, but we need to create them manually in the case of CLI

1. Create bootable volume from image(e.g. centos):
```
openstack volume create --image "centos-7-1809-x86_64" --size 40 my_vol1
```

## Create server

1. Create the instance:
```
openstack server create --flavor "standard.small" --volume my_vol1 \
 --key-name my-key1 --security-group my-security-group --network my-net1 my-server1
```

## Floating IP address management

### Creating and assigning new FIP

1. Allocate new Floating IPs:

```
$ openstack floating ip create public-cesnet-78-128-251
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field               | Value                                                                                                                                                                          |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| created_at          | 2019-06-06T06:56:51Z                                                                                                                                                           |
| description         |                                                                                                                                                                                |
| dns_domain          | None                                                                                                                                                                           |
| dns_name            | None                                                                                                                                                                           |
| fixed_ip_address    | None                                                                                                                                                                           |
| floating_ip_address | 78.128.251.27                                                                                                                                                                  |
| floating_network_id | 410e1b3a-1971-446b-b835-bf503917680d                                                                                                                                           |
| id                  | d054b6b3-bbd3-485d-a46b-b80682df8fc8                                                                                                                                           |
| location            | Munch({'cloud': '', 'region_name': 'brno1', 'zone': None, 'project': Munch({'id': 'f0c339b86ddb4699b6eab7acee8d4508', 'name': None, 'domain_id': None, 'domain_name': None})}) |
| name                | 78.128.251.27                                                                                                                                                                  |
| port_details        | None                                                                                                                                                                           |
| port_id             | None                                                                                                                                                                           |
| project_id          | f0c339b86ddb4699b6eab7acee8d4508                                                                                                                                               |
| qos_policy_id       | None                                                                                                                                                                           |
| revision_number     | 0                                                                                                                                                                              |
| router_id           | None                                                                                                                                                                           |
| status              | DOWN                                                                                                                                                                           |
| subnet_id           | None                                                                                                                                                                           |
| tags                | []                                                                                                                                                                             |
| updated_at          | 2019-06-06T06:56:51Z                                                                                                                                                           |
+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
```

2. And assign it to your server:

```
$ openstack server add floating ip  net-test1 78.128.251.27
```

### Remove existing floating IP

1. List your servers:

```
$ openstack server list
+--------------------------------------+-----------+--------+-------------------------------------------------------+-------+----------------+
| ID                                   | Name      | Status | Networks                                              | Image | Flavor         |
+--------------------------------------+-----------+--------+-------------------------------------------------------+-------+----------------+
| 1a0d4624-5294-425a-af37-a83eb0640e1c | net-test1 | ACTIVE | auto_allocated_network=192.168.8.196, 147.251.124.248 |       | standard.small |
+--------------------------------------+-----------+--------+-------------------------------------------------------+-------+----------------+
```

2. remove floating IPs:

```
$ openstack server remove floating ip  net-test 147.251.124.248
$ openstack floating ip delete 147.251.124.248
```


## Cloud tools
You can inspect cloud tools [here](/cloud/tools)

## Full Reference
See [OpenStack CLI Documentation](https://docs.openstack.org/python-openstackclient/train/).
