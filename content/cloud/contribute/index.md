---
title: "Contribute"
date: 2021-05-18T11:22:35+02:00
draft: false
weight: 110
---

We use the open-source [Hugo](https://gohugo.io/) project to generate the documentation.


## Requirements
[Install](https://gohugo.io/getting-started/installing/) Hugo


## Work-flow Overview
1. Fork & clone repository
2. Create a branch
3. Commit your changes
4. Push to the branch
5. Create a Merge Request with the content of your branch

### Fork Repository
See [GitLab @ ICS MU](https://gitlab.ics.muni.cz/cloud/documentation/forks/new) for details. This will create your own clone of our repository where you will be able to make changes. Once you are happy with your changes, use GitLab to submit them to our original repository.

### Clone Repository
```bash
# after creating your own copy of the repository on GitLab
git clone git@gitlab.ics.muni.cz:${GITLAB_USER}/documentation.git
```

### Create New Branch
```bash
# in `documentation`
git checkout -b my_change
```

### Make Changes & Run Local Server
```bash
# in `documentation`
hugo --config config-dev.toml serve
```
> Edits will be shown live in your browser window, no need to restart the server.

### Commit and Push Changes
```bash
git commit -am "My updates"
git push origin my_change
```

### Submit Changes
Create a *Merge Request* via [GitLab @ ICS MU](https://gitlab.ics.muni.cz/cloud/documentation/merge_requests/new).


## Tips
### Disable table of content
The table of content is generated automatically for every page. To hide the table of contents, put this line to the page's header:
```
disableToc: true
```

### Hide from the menu
To hide a page from the menu, add this line to the page's header:
```
GeekdocHidden: true
```


### Hints
To show "hint bar" similar to this one:
{{< hint info >}}
some text
{{</hint>}}
you can use *short codes*.

Please see [theme documentation](https://geekdocs.de/shortcodes/hints/).
