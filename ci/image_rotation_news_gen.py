#!/usr/bin/env python3

"""
Script generates dynamically image rotation news updates

Actions performed:
 generating of image rotation news 
 * if everything goes well
   - image rotation news section is updated according to image rotation journal


Usage:
  python3 ./image_rotation_news.gen.py
"""

import re
import datetime

DOC_NEWS_FILE = "content/cloud/news/index.md"
IMAGE_ROTATION_FILE = "content/cloud/image-rotation/index.md"


def get_image_update_log_dates():
    """Gets all image rotation update dates"""
    with open(IMAGE_ROTATION_FILE, "r", encoding="utf-8") as rotate_file:
        file_content = rotate_file.read()
        return re.findall(
            r"## Image rotation update from [0-9]{4}-[0-9]{2}-[0-9]{2}:", file_content
        )


def generate_news(image_update_dates):
    """Generates news with image update news"""
    with open(DOC_NEWS_FILE, "r", encoding="utf-8") as news_file:
        news_content = news_file.readlines()
        news_content = [line.strip() for line in news_content]

    for update_date in image_update_dates:
        update_date = datetime.datetime.strptime(
            update_date, "## Image rotation update from %Y-%m-%d:"
        )

        for line_idx, line in enumerate(news_content):
            if re.match(r".*\*\*[0-9]{4}-[0-9]{2}-[0-9]{2}\*\*.*", line):
                news_date = line.split("**")[1]
                news_date = datetime.datetime.strptime(news_date, "%Y-%m-%d")
                if update_date > news_date or line_idx == len(news_content) - 1:
                    update_date = datetime.datetime.strftime(update_date, "%Y-%m-%d")
                    new_entry = [
                        f"**{update_date}** Image rotation update, details [here](https://docs.cloud.muni.cz/"
                        f"cloud/image-rotation/#image-rotation-update-from-{update_date})",
                    ]
                    news_content = (
                        news_content[:line_idx] + new_entry + [""] + news_content[line_idx:]
                        if line_idx != len(news_content) - 1
                        else news_content + [""] + new_entry
                    )
                    break
    with open(DOC_NEWS_FILE, "w", encoding="utf-8") as news_file:
        news_file.write("\n".join(news_content))


if __name__ == "__main__":
    IMAGE_UPDATE_DATES = get_image_update_log_dates()
    if IMAGE_UPDATE_DATES:
        generate_news(IMAGE_UPDATE_DATES)
